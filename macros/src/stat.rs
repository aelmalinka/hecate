use syn::parse::{Parse, ParseStream, Result};
use syn::punctuated::Punctuated;
use syn::{Expr, ExprAssign, ExprPath, Token};

use crate::scaled::Scaled;
pub use crate::simple::Struct as Item;

pub struct Args {
    pub cost: Scaled,
}

impl Parse for Args {
    fn parse(input: ParseStream) -> Result<Self> {
        let vars = Punctuated::<Expr, Token![,]>::parse_terminated(input)?;

        let mut cost: Option<Scaled> = Option::None;

        for v in vars {
            match v {
                Expr::Assign(ExprAssign { left, right, .. }) => {
                    let Expr::Path(ExprPath { path: left, .. }) = Box::into_inner(left) else {
                        return Err(input.error("unknown expression {left}"));
                    };
                    if left.is_ident("cost") {
                        if cost.is_some() {
                            return Err(input.error("cost already specified"));
                        }

                        cost.replace(Scaled::parse(right, input)?);
                    } else {
                        return Err(input.error("unknown target {left}"));
                    }
                }
                _ => {
                    return Err(input.error("unexpected argument {v}"));
                }
            };
        }

        let cost = cost.ok_or_else(|| input.error("cost not specified"))?;

        Ok(Self { cost })
    }
}
