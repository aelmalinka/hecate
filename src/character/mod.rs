//! Character building blocks

pub mod holder;
pub mod resource;
pub mod traits;

pub use hecate_macros::{character, resource, stat};
pub use resource::Resource;
pub use traits::{Cost, Percentage, Transform};
