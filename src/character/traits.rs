//! basic utility types and traits for character
use std::ops::Deref;

/// Value used for most storage/calculation purposes
// 2021-09-12 AMR TODO: move to another submodule?
pub type Percentage = i64;

/// Something that adds cost to the Character
pub trait Cost: AsRef<Percentage> {
    /// The actual cost of `&self`
    fn cost(&self) -> Percentage;
}

/// Used to allow Resources to modify the `base_value` before returning
///     (i.e. 1 Constitution is 10 Health, etc)
pub trait Transform {
    /// The transform function
    fn transform(value: Percentage) -> Percentage;
}

// 2021-09-12 AMR TODO: should this be moved to default impl macro?
impl<T> Transform for T
// 2021-09-12 AMR NOTE: is a Stat/Skill not Resource (see ResourceRaw)
where
    T: Deref<Target = Percentage>,
{
    fn transform(value: Percentage) -> Percentage {
        value
    }
}

#[cfg(test)]
mod tests {
    use derive_more::Deref;

    use super::*;

    #[derive(Deref)]
    struct Test(Percentage);

    #[test]
    fn stat_has_default_transform() {
        assert_eq!(Test::transform(1), 1);
        assert_eq!(Test::transform(10), 10);
    }
}
