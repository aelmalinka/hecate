use proc_macro2::{Span, TokenStream};
use quote::{format_ident, quote, ToTokens};
use syn::parse::{Parse, ParseStream, Result};
use syn::{Error, Field as FieldTy, Ident, ItemStruct, Token, Type, Visibility};

pub struct Item {
    vis: Visibility,
    struct_token: Token![struct],
    ident: Ident,
    fields: Fields,
}

struct Field {
    ident: Ident,
    ty: Type,
    ignore: bool,
    bases: Bases,
}

struct FieldIdent<'a> {
    ident: &'a Ident,
    ty: &'a Type,
}

struct FieldIdents<'a>(Vec<FieldIdent<'a>>);
struct Fields(Vec<Field>);
struct Bases(Vec<Ident>);

impl Fields {
    fn parts(&self) -> (FieldIdents<'_>, impl Iterator<Item = &'_ Ident> + '_, &Self) {
        (
            FieldIdents(self.0.iter().map(FieldIdent::from).collect::<Vec<_>>()),
            self.0.iter().filter_map(
                |Field { ident, ignore, .. }| {
                    if *ignore {
                        None
                    } else {
                        Some(ident)
                    }
                },
            ),
            self,
        )
    }
}

impl Parse for Item {
    fn parse(input: ParseStream) -> Result<Self> {
        let item = ItemStruct::parse(input)?;

        let ItemStruct {
            vis,
            struct_token,
            ident,
            fields,
            ..
        } = item;

        let fields = Fields(
            fields
                .into_iter()
                .map(Field::try_from)
                .collect::<Result<Vec<_>>>()?,
        );

        Ok(Self {
            vis,
            struct_token,
            ident,
            fields,
        })
    }
}

impl ToTokens for Item {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let Self {
            vis,
            struct_token,
            ident,
            fields,
        } = self;

        let (fields, costs, impls) = fields.parts();

        // 2024-04-11 MLT TODO: pass through documentation comments?
        tokens.extend(quote! {
            /// The main character type of Hecate for this crate
            #[derive(Clone, Debug, Default)]
            #vis #struct_token #ident {
                #fields
            }

            impl #ident {
                /// Returns the current cost of this character
                pub fn cost(&self) -> Percentage {
                    0 #( + self.#costs.cost() )*
                }

                #impls
            }
        });
    }
}

impl ToTokens for FieldIdents<'_> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        self.0.iter().for_each(|i| {
            let FieldIdent { ident, ty } = i;

            tokens.extend(quote! {
                #ident: #ty,
            });
        });
    }
}

impl ToTokens for Fields {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        self.0.iter().for_each(|i| {
            let Field {
                ident,
                ty,
                ignore,
                bases,
            } = i;

            let get = ident;
            let set = format_ident!("{}_mut", ident);
            let len = bases.0.len();

            if *ignore {
                // 2024-04-11 MLT TODO: this isn't ignore and more data?
                tokens.extend(quote! {
                    /// fetches #ty by reference
                    pub fn #get(&self) -> &#ty {
                        &self.#ident
                    }
                    /// fetches #ty by mutable reference
                    pub fn #set(&mut self) -> &mut #ty {
                        &mut self.#ident
                    }
                });
            } else {
                tokens.extend(quote! {
                    /// fetches #ty using a [`::hecate::character::holder::Ref`]
                    pub fn #get(&self) -> ::hecate::character::holder::Ref<#ty, #len> {
                        ::hecate::character::holder::Ref::new(&self.#ident, #bases)
                    }
                    /// fetches #ty using a [`::hecate::character::holder::Mut`]
                    pub fn #set(&mut self) -> ::hecate::character::holder::Mut<#ty, #len> {
                        ::hecate::character::holder::Mut::new(&mut self.#ident, #bases)
                    }
                });
            }
        });
    }
}

impl ToTokens for Bases {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let i = self.0.iter();
        tokens.extend(quote! {
            [
                #( &self.#i , )*
            ]
        });
    }
}

impl TryFrom<FieldTy> for Field {
    type Error = Error;

    fn try_from(field: FieldTy) -> Result<Self> {
        let FieldTy {
            ident, ty, attrs, ..
        } = field;

        let ident = ident.ok_or_else(|| Error::new(Span::call_site(), "field isn't named"))?;
        let ignore = attrs.iter().any(|i| i.path().is_ident("ignore"));
        let mut bases = Bases(Vec::new());
        let b = &mut bases.0;

        attrs
            .into_iter()
            .filter(|i| i.path().is_ident("skill"))
            .try_for_each(|i| {
                i.parse_nested_meta(|m| {
                    b.push(m.path.require_ident()?.clone());

                    Ok(())
                })
            })?;

        Ok(Self {
            ident,
            ty,
            ignore,
            bases,
        })
    }
}

impl<'a> From<&'a Field> for FieldIdent<'a> {
    fn from(Field { ident, ty, .. }: &'a Field) -> Self {
        Self { ident, ty }
    }
}
