//! Resource utilities
use std::ops::{Deref, DerefMut};

#[cfg(feature = "bevy")]
use bevy::prelude::*;

use super::traits::Percentage;

/// The raw type for a resource
#[derive(Clone, Copy, Debug, Default)]
#[cfg_attr(feature = "bevy", derive(Reflect))]
pub struct Raw {
    current: Percentage,
    points: Percentage,
}

/// Models a resource
pub trait Resource {
    /// The current points
    fn current(&self) -> &Percentage;
    /// The number of points put into the resource
    fn points(&self) -> &Percentage;
    /// The current points (as mut)
    fn current_mut(&mut self) -> &mut Percentage;
    /// The number of points put into the resource (as mut)
    fn points_mut(&mut self) -> &mut Percentage;
    /// Refill the resource
    fn refill(&mut self) {
        *self.current_mut() = *self.points();
    }
}

impl Resource for Raw {
    fn current(&self) -> &Percentage {
        &self.current
    }

    fn points(&self) -> &Percentage {
        &self.points
    }

    fn current_mut(&mut self) -> &mut Percentage {
        &mut self.current
    }

    fn points_mut(&mut self) -> &mut Percentage {
        &mut self.points
    }
}

impl<T> Resource for T
where
    T: Deref<Target = Raw> + DerefMut<Target = Raw>,
{
    fn current(&self) -> &Percentage {
        self.deref().current()
    }

    fn points(&self) -> &Percentage {
        self.deref().points()
    }

    fn current_mut(&mut self) -> &mut Percentage {
        self.deref_mut().current_mut()
    }

    fn points_mut(&mut self) -> &mut Percentage {
        self.deref_mut().points_mut()
    }
}

impl AsRef<Percentage> for Raw {
    fn as_ref(&self) -> &Percentage {
        self.points()
    }
}

impl AsMut<Percentage> for Raw {
    fn as_mut(&mut self) -> &mut Percentage {
        self.points_mut()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn resource_raw_has_default() {
        let r = Raw::default();

        assert_eq!(r.current, 0);
        assert_eq!(r.points, 0);
    }
    #[test]
    fn resource_raw_has_asref() {
        let r = Raw::default();

        assert_eq!(*r.as_ref(), 0);
    }
    #[test]
    fn resource_raw_has_asmut() {
        let mut r = Raw::default();

        *r.as_mut() = 10;

        assert_eq!(*r.as_ref(), 10);
    }
    #[test]
    fn resource_raw_has_clone() {
        let r = Raw {
            current: 0,
            points: 5,
        };
        let s = r;

        assert_eq!(*s.current(), 0);
        assert_eq!(*s.points(), 5);
    }
    #[test]
    fn resource_raw_has_copy() {
        let r = &mut Raw {
            current: 0,
            points: 5,
        };
        let s = *r;

        assert_eq!(*s.current(), 0);
        assert_eq!(*s.points(), 5);
    }
    #[test]
    fn resource_raw_has_debug() {
        let r = format!("{:?}", Raw::default());

        assert!(!r.is_empty());
    }
    #[test]
    fn resource_muts() {
        let mut r = Raw::default();

        *r.current_mut() = 5;
        *r.points_mut() = 10;

        assert_eq!(*r.current(), 5);
        assert_eq!(*r.points(), 10);
    }
}
